package hr;

public class Employee extends Worker {
	
	//member variables
	private String employeeId;

	Employee(String firstName, String lastName, String location, String designation, String employeeId) {
		super(firstName, lastName, location, designation);
		this.employeeId = employeeId;
	}

	//getters and setters
	
	public String getEmployeeId() {
		return employeeId;
	}
	
	@Override
	public void calculateWage() throws Exception {
		// salary can be calculated by 
		// number of days worked within a year * by 8 hours * base depending on designation
		switch(super.getDesignation()){
			case "JUNIOR":
				super.setSalary((double) super.getDaysWorked() * 8 * 25);
				break;
			case "MID":
				super.setSalary((double) super.getDaysWorked() * 8 * 35);
				break;
			case "SENIOR":
				super.setSalary((double) super.getDaysWorked() * 8 * 45);
				break;
			case "LEADER":
				super.setSalary((double) super.getDaysWorked() * 8 * 50);
				break;
			case "MANAGER":
				super.setSalary((double) super.getDaysWorked() * 8 * 50);
				break;
			case "CEO":
				super.setSalary((double) super.getDaysWorked() * 8 * 100);
				break;
			default:
				super.setSalary(-1);
				throw new Exception ("can not find designation");
		}
		
	}

	@Override
	public void calculateTax() {
		if(super.getSalary() >= 0 && super.getSalary() <= 18200) {
			super.setTax(0);
		} else if (super.getSalary() >= 18201 && super.getSalary() <= 37000) {
			int delta = (int) (super.getSalary() - 18200);
			double tax = 0.19 * delta;
			super.setTax(tax);
			
		} else if (super.getSalary() >= 37001 && super.getSalary() <= 90000) {
			int delta = (int) (super.getSalary() - 37000);
			double tax = (0.325 * delta) + 3572 ;
			super.setTax(tax);
		} else if (super.getSalary() >= 90000 && super.getSalary() <= 180000) {
			int delta = (int) (super.getSalary() - 90000);
			double tax = (0.37 * delta) + 20797 ;
			super.setTax(tax);
			
		} else if (super.getSalary() >= 180001) {
			int delta = (int) (super.getSalary() - 180000);
			double tax = (0.45 * delta) + 54097 ;
			super.setTax(tax);
		} else {
			super.setTax(-1);
		}
		
	}

	@Override
	public void calculateSuper() {
		// super is calculated by gross salary multiplied by 9.5%
		double superanuation = super.getSalary() * 0.095;
		if( superanuation >= 0) {
			super.setSuperanution(superanuation);
		} else {
			super.setSuperanution(-1);
		}
		
	}
	
	

}
