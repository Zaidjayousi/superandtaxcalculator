package hr;

public class Tester {

	public static void main(String[] args) {
		Employee employee = new Employee("Jack", "Rayleigh", "Sydney", "JUNIOR", "0000001");
		employee.setSalary(19000);
		employee.calculateTax();
		employee.calculateSuper();
		Helper.generate(employee);
		
		Employee employee1 = new Employee("Austin", "Powers", "Sydney", "CEO", "0000002");
		employee1.setSalary(18201);
		employee1.calculateTax();
		employee1.calculateSuper();
		Helper.generate(employee1);
		
		Employee employee2 = new Employee("Ronald", "McDonalad", "Sydney", "CLOWN", "0000003");
		employee2.setDaysWorked(200);;
		try {
			employee2.calculateWage();
		} catch(Exception e) {
			System.out.println("****************");
			System.out.print(e.getMessage() +  " ");
			
		} 
		employee2.calculateTax();
		employee2.calculateSuper();
		Helper.generate(employee2);

	}

}

class Helper {
	public static void generate(Worker worker) {
		if( worker instanceof Employee ) {
			System.out.println("Employee");
			System.out.println("****************");
			System.out.println("Name: "+ worker.getFullName());
			System.out.println("Designation: " + worker.getDesignation());
			System.out.println("Salary: "+ worker.getSalary());
			System.out.println("tax: "+ worker.getTax());
			System.out.println("Super: "+ worker.getSuperanution());
			System.out.println();
			
		}
	}
}
