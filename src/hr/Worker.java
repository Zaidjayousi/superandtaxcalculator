package hr;

public abstract class Worker {
	// member variables
	private String firstName;
	private String lastName;
	private double salary;
	private int daysWorked;
	private String location;
	private String designation;
	private boolean employmentStatus;
	private double tax;
	private double superanution;
	
	// parameterised constructor
	Worker(String firstName, String lastName, String location, String designation) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.location = location;
		this.designation = designation;
	}
	
	// abstract methods
	public abstract void calculateWage() throws Exception;
	
	public abstract void calculateTax();
	
	public abstract void calculateSuper();
	
	// methods
	public String getFullName() {
		return firstName + " " + lastName;
	}
	
	// getters and setters

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public boolean isEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(boolean employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public int getDaysWorked() {
		return daysWorked;
	}
	
	public void setDaysWorked(int daysWorked) {
		this.daysWorked = daysWorked;
	}

	public String getLocation() {
		return location;
	}

	public String getDesignation() {
		return designation;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getSuperanution() {
		return superanution;
	}

	public void setSuperanution(double superanution) {
		this.superanution = superanution;
	}
	
	
}
