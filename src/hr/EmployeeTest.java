package hr;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EmployeeTest {
	

	@Test
	void testCalculateTax() {
		Employee employee = new Employee("Jack", "Rayleigh", "Sydney", "JUNIOR", "0000001");
		employee.setSalary(19000);
		employee.calculateTax();
		employee.calculateSuper();
		// (19,000 - 18,200) * 0.19
		assertEquals(employee.getTax(), 152);
	}
	
	@Test
	void testCalculateSuper() {
		Employee employee = new Employee("Jack", "Rayleigh", "Sydney", "JUNIOR", "0000001");
		employee.setSalary(19000);
		employee.calculateTax();
		employee.calculateSuper();
		// 19,000 * 0.095
		assertEquals(employee.getSuperanution(), 1805);
	}
	
	// edge cases
	@Test
	void testEdgeCalculateSuper() {
		Employee employee = new Employee("Jack", "Rayleigh", "Sydney", "JUNIOR", "0000001");
		employee.setSalary(200000);
		employee.calculateTax();
		employee.calculateSuper();
		// 200,000 * 0.095
		assertEquals(employee.getSuperanution(), 19000);
	}
	
	@Test
	void testEdgeCalculateTax() {
		Employee employee = new Employee("Jack", "Rayleigh", "Sydney", "JUNIOR", "0000001");
		employee.setSalary(200123.95);
		employee.calculateTax();
		employee.calculateSuper();
		// ((200,123.95 - 180,000) * 0.45) + 54097
		// 20,123.00 *0.45 + 54097
		assertEquals(employee.getTax(), 63152.35, 0.1);
	}

}
